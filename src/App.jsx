
import React from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import './App.css';
import logosvg from './img/MyLogo.svg';
import IconMenuSvg from './img/LogoMenu.svg';
import Menu from './Components/Menu/Menu' 
import Prodazhy from './Components/Screen/Prodazhy';
import Zakazy from './Components/Screen/Zakazy';
import Debet from './Components/Screen/Debet';
import Sklad from './Components/Screen/Sklad';
import Analys from './Components/Screen/Analys';
import Magaziny from './Components/Screen/Magaziny';
import Abc from './Components/Screen/Abc';
import Tovary from './Components/Screen/Tovary';
import TovaryMagaz from './Components/Screen/TovaryMagaz';
import Mag from './Components/Screen/Mag';
import Zagl from './Components/Screen/Zagl';
import OstatkiManager from './Components/Screen/OstatkiManager';

function App() {
  return (
    <Router>
    <div className = "MyApp">
      <div className = "header-app">
        <Link className = "link-logo" to="/"><img src={logosvg} alt="Easy data"/></Link>
        <Link className = "link-menu" to="/"><img className = "icon-menu-svg" src={IconMenuSvg} alt="Menu"/></Link>
      </div>
      
      <Route exact path="/" component={Menu} />
      <Route path="/prodazhy" component={Prodazhy} />
      <Route path="/ostatki" component={Sklad} />
      <Route path="/zakazy" component={Zakazy} />
      <Route path="/debet" component={Debet} />
      <Route exact path="/analys" component={Analys} />
      <Route exact path="/magaziny" component={Magaziny} />
      <Route exact path="/magaziny/sum" component={Mag} />
      <Route exact path="/magaziny/tov" component={TovaryMagaz} />
      <Route exact path="/analys/abc" component={Abc} />
      <Route exact path="/analys/xyz" component={Zagl} />
      <Route exact path="/tovary" component={Tovary} />
      <Route exact path="/zagl" component={Zagl} />
      <Route exact path="/OstatkiManager" component={OstatkiManager} />

    </div>
    </Router>
  );
}
export default App;