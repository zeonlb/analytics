import React, { Component } from 'react';

function FetchApp(props, state) {
  console.log(props, state);
  var value: [];
  var isLoading: false;
  var error: null;
    fetch(props)
      .then(response => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error('Ошибка подключения ...');
        }
      }).then(response => ({ value: response.value, isLoading: true }));
      console.log(value);
      // .then(response => this.setState({ value: response.value, isLoading: true }));
      // .catch(error => this.setState({ error, isLoading: false }));
      
    return {value, isLoading};
}

export default FetchApp;