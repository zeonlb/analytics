import React from 'react';
//import { render } from 'react-dom';
import { slideDown, slideUp } from './anim';
import './Ostatki.css';
//import { ta } from 'date-fns/locale';


class SkladTableRow extends React.Component {
  state = { expanded: false }

  toggleExpander = (e) => {
    if (!this.state.expanded) {
      this.setState(
        { expanded: true },
        () => {
          if (this.refs.expanderBody) {
            slideDown(this.refs.expanderBody);
          }
        }
      );
    } else {
      slideUp(this.refs.expanderBody, {
        onComplete: () => { this.setState({ expanded: false }); }
      });
    }
  }

  render() {
    const {keys, datakat, kategory, mag } = this.props;
    console.log(datakat);
    return [
      <div key={keys} onClick={this.toggleExpander}>
        <h5 className="kategory">{kategory}</h5>
      </div>,
      this.state.expanded && (
        <div className="expandable" key="tr-expander">
          <div className="uk-background-muted">
            <div ref="expanderBody" className="inner uk-grid">
            <table className="table-responsive">
              <tr className="table-stroka-zagolovok">
                <th className="table-kolonka1-zagolovok">Наименование</th>
                {mag.map((item, index)=><th className="table-kolonki-zagolovok">{item}</th>)}
              </tr>
              {Object.keys(datakat).map((item, index) => <tr key={index} className="table-stroka-telo"><td className="table-kolonka1-telo">{item}</td>{mag.map((item2, index2)=><td className="table-kolonki-telo">{datakat[item][item2] === undefined ? 0 : datakat[item][item2]}</td>)}</tr>)}
            </table>          
            </div>
          </div>
        </div>
      )
    ];
  }
}



class App extends React.Component {
  state = { sklad: [] }

  componentDidMount() {
    fetch(api + '/pkk/hs/pkk/pkr')
      .then(response => response.json())
      .then(data => { this.setState({sklad: data.value}) });
  }

  render() {
    const { sklad } = this.state;
    const uniMag = [...new Set(sklad.map(item => item.Магазины))];
    if(sklad.length > 0){
      var grid = {};
      sklad.map((item, index) => {
        if(item.Категории !== 'Продукция' 
        && item.Категории !== 'Архив' 
        && item.Категории !== 'АРХИВ ПИВО' 
        && item.Категории !== 'АРХИВ КОФЕ/ЧАЙ' 
        && item.Категории !== 'АРХИВ ЗАКУСКА' 
        && item.Категории !== '12.Кофе Чай' 
        && item.Категории !== '13.ОБОРУДОВАНИЕ' 
        && item.Категории !== '14.АРХИВ' 
        && item.Категории !== 'АРХИВ ВИНО' 
        && item.Категории !== '13.Оборудование' 
        && item.Категории !== '17.АРХИВ' 
        && item.Категории !== '10.Тара' 
        && item.Категории !== '14. Хоз. товары'){
          var kategory = item.Категории;
          if(kategory in grid === false){
              grid[kategory] = {};
          };
          var tovary = item.Товары;
          if(tovary in grid[kategory] === false){
              grid[kategory][tovary] = {};
          };
          grid[kategory][item.Товары][item.Магазины] = item.Количество + item.ЕдиницаИзмерения;
        }
      })
      return (
        <div className="conteiner-list">
          {
            Object.keys(grid).map((item, index) =>
              <SkladTableRow key={index} keys={index} kategory={item} datakat={grid[item]} mag={uniMag}/>
            )
          }
        </div>  
      );
    };
    return (
      <div className="conteiner-list">
        <div  className="text-center"><em className="text-muted">Загрузка остатков...</em></div>
      </div>  
    );
  }
}
export default App;
