import React, { Component } from 'react';
import { slideDown, slideUp } from './anim';
import Button from '@material-ui/core/Button';
import loading from '../../img/Loading.gif';
import './Debet.css';

class SkladTableRow extends React.Component {
  state = { expanded: false }
  toggleExpander = (e) => {
    if (!this.state.expanded) {
      this.setState(
        { expanded: true },
        () => {
          if (this.refs.expanderBody) {
            slideDown(this.refs.expanderBody);
          }
        }
      );
    } else {
      slideUp(this.refs.expanderBody, {
        onComplete: () => { this.setState({ expanded: false }); }
      });
    }
  }
  
  render() {
    const {keys, dataitem, kontragents } = this.props;
    //   console.log('-----------------');
    //   Object.keys(dataitem).map((item, index) =>console.log(item))
    //   console.log('-----------------');
    var TotalSumm =  ~~dataitem['Total'];
    TotalSumm = TotalSumm.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ');
    return [
      <Button key={keys} onClick={this.toggleExpander} className="buttontext">
        <span className="spanlabel">
          <div className="kat">
            <div className="agent">{kontragents}</div>
            <div className="total">
              {dataitem['Total'] < 0 ? <p className="p">Общая сумма долга: <span className="sr">{TotalSumm}</span> грн.</p> : <p className="p">Долг погашен с переплатой: <span className="sg">{TotalSumm}</span> грн.</p>}
            </div>
          </div>
          <svg className="MuiSvgIcon-root" focusable="false" viewBox="0 0 24 24" aria-hidden="true" role="presentation"><path d="M16.59 8.59L12 13.17 7.41 8.59 6 10l6 6 6-6z"></path></svg>
        </span>
      </Button>,
      this.state.expanded && (
        <div className="expandable" key="tr-expander">
          <div className="uk-background-muted">
            <div ref="expanderBody" className="inner">
              <div className="grid-table-debet">
                  <div className="kol zag1">Дата поставки</div>
                  <div className="kol zag2">Вид платежа</div>
                  <div className="kol zag3">Сумма</div>
                  {Object.keys(dataitem).map((item, index) => {if(item !== 'Total'){return[
                      <div key={index} className="kol tel1">{dataitem[item].Data}</div>,
                      <div className="kol tel2">{dataitem[item].Vid}</div>,
                      <div className="kol tel3">{dataitem[item].Summ}</div>
                      ]}})}
              </div> 
            </div>
          </div>
        </div>
      )
    ];
  }
};
class Debet extends React.Component {
    state = { debet: [],
              error: null }
  
    componentDidMount() {
      fetch(&api + '/pkk/hs/pkk/debet')
        .then(response => response.json())
        .then(data => { this.setState({debet: data.value}) })
        .catch(error => this.setState({ error, isLoading: false }));
    }
  
    render() {
      const { debet, error } = this.state;
      
      if(debet.length > 0){
        var grid = {};
        var val = '';
        var totalSumm = 0;
        debet.map((item, index) => {
            var postavsik = item.Поставщик;
            if( val !== postavsik){
                totalSumm = 0;
                val = postavsik;
            }
            if(postavsik in grid === false){
                grid[postavsik] = {};
            };
            if(index in grid[postavsik] === false){
                grid[postavsik][index] = {};
            };
            if(index in grid[postavsik] === false){
                grid[postavsik][index] = {};
            };
            grid[postavsik][index] = {Data: item.Дата, Vid: item.ВидРасчета, Summ: item.Сумма};
            totalSumm = totalSumm + item.Сумма;
            grid[postavsik]['Total'] = totalSumm;
        });
        //Вывод сообщения в случае ошибки
        if(error !== null){
          return (
              <>
              <h1>Warning !</h1>
              <p>Connection failed</p>
              <p>You have problem with internet</p>
              <p>Server is not response</p>
              </>
          )
          };
        return (
          <div className="conteiner-list">
            <p className="card-title"><span>Дебиторская</span> Задолжность</p>
            {
              Object.keys(grid).map((item, index) =>
                <SkladTableRow key={index} keys={index} kontragents={item} dataitem={grid[item]}/>
              )
            }
          </div>  
        );
      };
      return (
        <div className="conteiner-list">
          <p className="card-title"><span>Дебиторская</span> Задолжность</p>
          <div  className="text-center"><em className="text-muted">Загрузка задолжностей...</em></div>
          <img className = "loading" src={loading}/>
        </div>  
      );
    }
  }
  export default Debet;