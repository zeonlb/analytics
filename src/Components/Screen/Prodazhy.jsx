import React, { Component } from 'react';
import './Prodazhy.css';
import DatePicker from "react-datepicker";
import { registerLocale } from  "react-datepicker";
import ru from 'date-fns/locale/ru';
import Widget from '../widget/widget';
import LineGraph from '../LineGraph/LineGraph';
registerLocale('ru', ru);

class Prodazhy extends Component {
  constructor() {
    super();
    this.state = {
      value: [],
      coast: [],
      chartYear: [],
      chartYear2: [],
      isLoading: false,
      error: null,
      startDate: new Date(),
      endDate: new Date(),
    };
  }
  
  zapros = () => {
    this.setState({isLoading: true});
    const startDate = this.state.startDate.getFullYear() + '-' + ('0' + (this.state.startDate.getMonth() + 1)).slice(-2) + '-' + ('0' + this.state.startDate.getDate()).slice(-2) + 'T00:00:00';
    const endDate = this.state.endDate.getFullYear() + '-' + ('0' + (this.state.endDate.getMonth() + 1)).slice(-2) + '-' + ('0' + this.state.endDate.getDate()).slice(-2) + 'T23:59:59';
    const API = api + '/pkk/odata/standard.odata/';
    const PRODAZHI_QUERY = 'Document_%D0%9E%D1%82%D1%87%D0%B5%D1%82%D0%9E%D0%A0%D0%BE%D0%B7%D0%BD%D0%B8%D1%87%D0%BD%D1%8B%D1%85%D0%9F%D1%80%D0%BE%D0%B4%D0%B0%D0%B6%D0%B0%D1%85?$format=json;odata=nometadata&$filter=Date%20ge%20datetime%27' + startDate + '%27%20and%20Date%20le%20datetime%27' + endDate + '%27%20and%20ОтражатьВБухгалтерскомУчете%20eq%20false&$select=%D0%A1%D0%BA%D0%BB%D0%B0%D0%B4_Key,%D0%A1%D1%83%D0%BC%D0%BC%D0%B0%D0%94%D0%BE%D0%BA%D1%83%D0%BC%D0%B5%D0%BD%D1%82%D0%B0&$orderby=%D0%A1%D1%83%D0%BC%D0%BC%D0%B0%D0%94%D0%BE%D0%BA%D1%83%D0%BC%D0%B5%D0%BD%D1%82%D0%B0%20desc';
    const SEBESTOIMOST_QUERY = 'AccumulationRegister_%D0%9F%D1%80%D0%BE%D0%B4%D0%B0%D0%B6%D0%B8%D0%A1%D0%B5%D0%B1%D0%B5%D1%81%D1%82%D0%BE%D0%B8%D0%BC%D0%BE%D1%81%D1%82%D1%8C_RecordType?$format=json;odata=nometadata&$filter=Period%20gt%20datetime%27' + startDate + '%27%20and%20Period%20lt%20datetime%27' + endDate + '%27&$select=%D0%A1%D1%82%D0%BE%D0%B8%D0%BC%D0%BE%D1%81%D1%82%D1%8C';
    //const {value, isLoading} = FetchApp(API + DEFAULT_QUERY, this.state);
    
    fetch(API + PRODAZHI_QUERY)
      .then(response => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error('Что-то пошло нетак ...');
        }
      })
      .then(data => this.setState({ value: data.value, isLoading: false }))
      .catch(error => this.setState({ error, isLoading: false }));
      
      fetch(API + SEBESTOIMOST_QUERY)
      .then(response => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error('Что-то пошло нетак ...');
        }
      })
      .then(data => this.setState({ coast: data.value, isLoading: false }))
      .catch(error => this.setState({ error, isLoading: false }));

  }

  componentDidMount() {
    this.zapros();
    const API = api + '/pkk/odata/standard.odata/';
    const CHART_YEAR_QUERY = 'Document_%D0%9E%D1%82%D1%87%D0%B5%D1%82%D0%9E%D0%A0%D0%BE%D0%B7%D0%BD%D0%B8%D1%87%D0%BD%D1%8B%D1%85%D0%9F%D1%80%D0%BE%D0%B4%D0%B0%D0%B6%D0%B0%D1%85?$format=json;odata=nometadata&$filter=Date%20ge%20datetime%272018-01-01T00:00:00%27%20and%20Date%20lt%20datetime%272018-12-31T23:59:59%27%20and%20%D0%9E%D1%82%D1%80%D0%B0%D0%B6%D0%B0%D1%82%D1%8C%D0%92%D0%91%D1%83%D1%85%D0%B3%D0%B0%D0%BB%D1%82%D0%B5%D1%80%D1%81%D0%BA%D0%BE%D0%BC%D0%A3%D1%87%D0%B5%D1%82%D0%B5%20eq%20false%20&$select=%D0%A1%D1%83%D0%BC%D0%BC%D0%B0%D0%94%D0%BE%D0%BA%D1%83%D0%BC%D0%B5%D0%BD%D1%82%D0%B0,Date&$orderby=Date%20asc';
    const CHART_YEAR_QUERY2 = 'Document_%D0%9E%D1%82%D1%87%D0%B5%D1%82%D0%9E%D0%A0%D0%BE%D0%B7%D0%BD%D0%B8%D1%87%D0%BD%D1%8B%D1%85%D0%9F%D1%80%D0%BE%D0%B4%D0%B0%D0%B6%D0%B0%D1%85?$format=json;odata=nometadata&$filter=Date%20ge%20datetime%272019-01-01T00:00:00%27%20and%20Date%20lt%20datetime%272019-12-31T23:59:59%27%20and%20%D0%9E%D1%82%D1%80%D0%B0%D0%B6%D0%B0%D1%82%D1%8C%D0%92%D0%91%D1%83%D1%85%D0%B3%D0%B0%D0%BB%D1%82%D0%B5%D1%80%D1%81%D0%BA%D0%BE%D0%BC%D0%A3%D1%87%D0%B5%D1%82%D0%B5%20eq%20false%20&$select=%D0%A1%D1%83%D0%BC%D0%BC%D0%B0%D0%94%D0%BE%D0%BA%D1%83%D0%BC%D0%B5%D0%BD%D1%82%D0%B0,Date&$orderby=Date%20asc';
    
    fetch(API + CHART_YEAR_QUERY)
      .then(response => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error('Что-то пошло нетак ...');
        }
      })
      .then(data => this.setState({ chartYear: data.value, isLoading: false }))
      .catch(error => this.setState({ error, isLoading: false }));

      fetch(API + CHART_YEAR_QUERY2)
      .then(response => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error('Что-то пошло нетак ...');
        }
      })
      .then(data => this.setState({ chartYear2: data.value, isLoading: false }))
      .catch(error => this.setState({ error, isLoading: false }));
  };
  
  componentDidUpdate(prevProps,prevState) {
    if (this.state.startDate.toDateString() !== prevState.startDate.toDateString() || this.state.endDate.toDateString() !== prevState.endDate.toDateString()) {
      this.zapros();
    }
  }

  render() {
    const { value, coast, chartYear, chartYear2, isLoading, error, startDate, endDate} = this.state;
    const StartInput = ({ value, onClick }) => (
      <div className="date-box">
        <span>от:</span>
        <button className="box-input" onClick={onClick}><span></span>
          {value}
        </button>
      </div>
    );
    const EndInput = ({ value, onClick }) => (
      <div className="date-box">
        <span>до:</span>
        <button className="box-input" onClick={onClick}><span></span>
          {value}
        </button>
      </div>
    );
    let labels = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"]
    let Summa = 0;
    let Summa2 = 0;
    var ArraySumma = [];
    var ArraySumma2 = [];
    if (chartYear.length > 0){
      for (var i = 0; i < chartYear.length; i++) {
        Summa += chartYear[i].СуммаДокумента;
        if(i < (chartYear.length - 1)){
          if(chartYear[i].Date.substr(0, 7) !== chartYear[i+1].Date.substr(0, 7)){
             //ArraySumma.push({'Month': chartYear[i].Date.substr(5, 2) , 'Summa': Summa});
             ArraySumma.push(Summa|0);
             Summa = 0;
          }; 
        }else{
          //ArraySumma.push({'Month': chartYear[i].Date.substr(5, 2) , 'Summa': Summa});
          ArraySumma.push(Summa|0);
        };  
      }; 
    };
    if (chartYear2.length > 0){
      for (var i = 0; i < chartYear2.length; i++) {
        Summa2 += chartYear2[i].СуммаДокумента;
        if(i < (chartYear2.length - 1)){
          if(chartYear2[i].Date.substr(0, 7) !== chartYear2[i+1].Date.substr(0, 7)){
             //ArraySumma.push({'Month': chartYear[i].Date.substr(5, 2) , 'Summa': Summa});
             ArraySumma2.push(Summa2|0);
             Summa2 = 0;
          }; 
        }else{
          //ArraySumma.push({'Month': chartYear[i].Date.substr(5, 2) , 'Summa': Summa});
          ArraySumma2.push(Summa2|0);
        };  
      }; 
    };
    
    let total = 0;
    let summ = 0;
    value.forEach((elem) => summ += elem.СуммаДокумента)
    let SumSales = summ;
    summ = Math.round(summ).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ') + " ₴";

    let summ2 = 0;
    coast.forEach((elem) => summ2 += elem.Стоимость)
    let SumFirstCost = summ2;
    total = SumSales - SumFirstCost;
    let procent1 = ((total/SumSales)*100)|0;
    let procent2 = (((total/SumFirstCost)*100))|0;
    summ2 = Math.round(summ2).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ') + " ₴";
    total = Math.round(total).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ') + " ₴";
    return (
      <div>
        <p className="card-title"><span>Данные</span> за период</p>
        <section className="date-section">
          <DatePicker 
            selected={startDate} 
            onChange={this.handleChangeStart} 
            maxDate={new Date()} 
            locale="ru"
            dateFormat="dd/MM/yyyy"
            todayButton="Сегодня"
            selectsStart
            startDate={startDate}
            endDate={endDate}
            withPortal
            fixedHeight
            customInput={<StartInput />}
          />
          <DatePicker 
            selected={endDate} 
            onChange={this.handleChangeEnd}
            maxDate={new Date()} 
            locale="ru"
            dateFormat="dd/MM/yyyy"
            todayButton="Сегодня"
            selectsEnd
            endDate={endDate}
            minDate={startDate}
            withPortal
            fixedHeight
            customInput={<EndInput />}
          />
        </section>
        <div className = "App">
          <Widget title1='Сумма' title2='Продаж' text='общая выручка по магазинам' value={isLoading ? 'Загрузка..' : summ} subvalue={isLoading ? 'Загрузка..' : summ2 + ' - себестоимость'}/>
          <Widget title1="Валовая" title2="Прибыль" text="за вычетом себестоимости" value={total} subvalue={procent1 + '% рентаб. | ' + procent2 + '% наценка'}/>
          <LineGraph data1={ArraySumma} data2={ArraySumma2} labels={labels}/>
        </div>
      </div>
    );
  }
  handleChangeStart = startDate => {
    this.setState({
      startDate
    });
  };
  handleChangeEnd = endDate => {
    this.setState({
      endDate
    });
  };
}

export default Prodazhy;