import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import DatePicker from "react-datepicker";
import loading from '../../img/Loading.gif';
import Widget from '../widget/widget';
//import "react-datepicker/dist/react-datepicker.css";
import { registerLocale } from  "react-datepicker";
import ru from 'date-fns/locale/ru';
import { slideDown, slideUp } from './anim';
import Button from '@material-ui/core/Button';
//import FetchApp from '../requests/fetchapp'
import './Magaziny.css';
registerLocale('ru', ru);
class SkladTableRow extends React.Component {
    state = { expanded: false}
    toggleExpander = (e) => {
      if (!this.state.expanded) {
        this.setState(
          { expanded: true },
          () => {
            if (this.refs.expanderBody) {
              slideDown(this.refs.expanderBody);
            }
          }
        );
      } else {
        slideUp(this.refs.expanderBody, {
          onComplete: () => { this.setState({ expanded: false }); }
        });
      }
    }
  
    render() {
        const {keys, datamag, magaziny } = this.props;
        delete datamag['SumIrarh'];
      
    //   const vv = datakat[Object.keys(datakat)[0]].Moment.split("; Внутренний заказ ");
    //   const Moment = vv[1];
      

    //   if(datamag !== undefined || datamag !== null){
        var ItogSumm = 0;
        var ItogBank = 0;
        var ItogSkidka = 0;
        Object.keys(datamag).map((item, index) => {
            if(datamag[item].Summ !== '') {ItogSumm = ItogSumm + datamag[item].Summ;};
            if(datamag[item].Bank !== '') {ItogBank = ItogBank + datamag[item].Bank;};
            if(datamag[item].Skidka !== '') {ItogSkidka = ItogSkidka + datamag[item].Skidka;};
        });
     
        ItogSumm = ItogSumm|0;
        ItogBank = ItogBank|0;
        ItogSkidka = ItogSkidka|0;

        const korrect = ItogSumm > 1 ? ItogSumm.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ') : '0';
        const korrect2 = ItogBank > 1 ? ItogBank.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ') : '0'; 
        const korrect3 = ItogSkidka > 0 ? ItogSkidka.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ') : '0';      
       
        return [
            <Button key={keys} onClick={this.toggleExpander} className="buttontext">
                {/* <div className="stroka">
                    <div className="text">
                        <div className="zagolovok">
                            <span className="imya">{magaziny}</span>
                        </div>
                        <div className="info">
                            <div className="summa rr"><span>Всего: </span><span className="chislo">{korrect} ₴</span></div>
                            <div className="summa ot"><span>Безнал: </span><span className="chislo">{korrect2} ₴</span></div>
                            <div className="summa ot"><span>Скидка: </span><span className="chislo">{korrect3} ₴</span></div>
                        </div>
                    </div>
                    <svg className="MuiSvgIcon-root" focusable="false" viewBox="0 0 24 24" aria-hidden="true" role="presentation"><path d="M16.59 8.59L12 13.17 7.41 8.59 6 10l6 6 6-6z"></path></svg>
                </div> */}
                <div className="stroka2">
                    <div className="text2">
                        
                        <div className="info-tovary">
                            <div className="zagolovok-tovary"><span className="imya2">{magaziny}</span></div>
                            <div className="summa-tovary"><span className="chislo">{korrect}</span></div>
                            <div className="summa-tovary"><span className="chislo">{korrect2}</span></div>
                            <div className="summa-tovary"><span className="chislo">{korrect3}</span></div>
                        </div>
                    </div>
                    <svg className="MuiSvgIcon-root" focusable="false" viewBox="0 0 24 24" aria-hidden="true" role="presentation"><path d="M16.59 8.59L12 13.17 7.41 8.59 6 10l6 6 6-6z"></path></svg>
                </div>
            </Button>,
          this.state.expanded && (
            <div className="expandable" key="tr-expander">
              <div  className="uk-background-muted">
                <div ref="expanderBody" className="inner">
               
                <div className="grid-table-magazin">
                    <div className="z1">Дата</div>
                    <div className="z">Сумма</div>
                    <div className="z">Безнал</div>
                    <div className="z">Скидка</div>            
                    
                    {Object.keys(datamag).map((item, index) => [
                        <div key={index} className="k t">{datamag[item].Date}</div>,
                        <div className="k t">{datamag[item].Summ|0}</div>,
                        <div className="k t">{datamag[item].Bank|0}</div>,
                        <div className="k t">{datamag[item].Skidka|0}</div>
                        ])}
                </div> 
                   
                </div>
              </div>
            </div>
          )
        ];
    }
  }

class Magaziny extends Component {
    constructor() {
        super();
        this.state = {
            value: [],
            startDate: new Date()-(7*24*60*60*1000),
            endDate: new Date()-(1000),
            isLoading: true,
            error: null,
        };
    };
    zapros = () => {
        this.setState({isLoading: true});
        const startDate = this.state.startDate;
        const endDate = this.state.endDate;
        const API = &api + '/pkk/hs/pkk/magaz/?dateb=' + startDate + '&datee=' + endDate;
        
        fetch(API)
          .then(response => {
            if (response.ok) {
              return response.json();
            } else {
              throw new Error('Что-то пошло нетак ...');
            }
          })
          .then(data => this.setState({ value: data.value, isLoading: false }))
          .catch(error => this.setState({ error, isLoading: false }));    
      };

    componentDidMount() {
        this.zapros();
    };
  
    componentDidUpdate(prevProps,prevState) {
        if (this.state.startDate !== prevState.startDate || this.state.endDate !== prevState.endDate) {
            this.zapros();
        }
    };
 
    render() {
        const { value, startDate, endDate, isLoading, error } = this.state;
        const back = '< Назад';
        const StartInput = ({ value, onClick }) => (
            <div className="date-box">
              <span>от:</span>
              <button className="box-input" onClick={onClick}><span></span>
                {value}
              </button>
            </div>
          );
          const EndInput = ({ value, onClick }) => (
            <div className="date-box">
              <span>до:</span>
              <button className="box-input" onClick={onClick}><span></span>
                {value}
              </button>
            </div>
          );
        //Вывод сообщения в случае ошибки
        if(value.length > 0 && isLoading === false){
            var grid = {};
            var valSumm = 0;
            var bankSumm = 0;
            var skidkaSumm = 0;
            //const uniMag = [...new Set(value.map(item => item.Магазин))];
            // const array4 = [skladCentre[0].Магазины, ...uniMag];
            // const array3 = [...skladCentre, ...sklad];
            var GlSumm = 0;
            var summaIrarhia = 'SumIrarh';

            value.map((item, index) => {
              if(item.СуммаДокумента !== '') valSumm = valSumm + item.СуммаДокумента;
              if(item.КартаБанкСумма !== '') bankSumm = bankSumm + item.КартаБанкСумма;
              if(item.СуммаСкидки !== '') skidkaSumm = skidkaSumm + item.СуммаСкидки;
              
              var Magazin = item.СкладНаименование;

              if(Magazin in grid === false){
                  grid[Magazin] = {};
              };
              if(summaIrarhia in grid[Magazin] === false){
                  grid[Magazin][summaIrarhia] = {};
                  GlSumm = 0;
              };
              GlSumm = GlSumm + item.СуммаДокумента;
              grid[Magazin][summaIrarhia] = GlSumm;
              if(index in grid[Magazin] === false){
                  grid[Magazin][index] = {};
              };
              grid[Magazin][index] = {
                Date: item.Дата,
                Summ: item.СуммаДокумента,
                Skidka: item.СуммаСкидки,
                Bank: item.КартаБанкСумма
              };
            });
            var result = {};
            Object.keys(grid).sort(function (a, b) {
                return grid[b].SumIrarh - grid[a].SumIrarh;
            }).forEach(function (v) { result[v] = grid[v]; });
            
            valSumm = valSumm|0;
            bankSumm = bankSumm|0;
            skidkaSumm = skidkaSumm|0;

            const hhh = valSumm.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ') + ' ₴';
            const hhh2 = bankSumm.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ') + ' ₴';
            const hhh3 = skidkaSumm.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ') + ' ₴';
            return (

              <div className="conteiner-list">
                <Link className = "link-item" to="/magaziny">
                    <div className="back">
                        {back}
                    </div>
                </Link>
                <p className="card-title"><span>Данные</span> за период</p>
                <section className="date-section">
                <DatePicker 
                    selected={startDate} 
                    onChange={this.handleChangeStart} 
                    maxDate={new Date()} 
                    locale="ru"
                    dateFormat="dd/MM/yyyy"
                    todayButton="Сегодня"
                    selectsStart
                    startDate={startDate}
                    endDate={endDate}
                    withPortal
                    fixedHeight
                    customInput={<StartInput />}
                />
                <DatePicker 
                    selected={endDate} 
                    onChange={this.handleChangeEnd}
                    maxDate={new Date()} 
                    locale="ru"
                    dateFormat="dd/MM/yyyy"
                    todayButton="Сегодня"
                    selectsEnd
                    endDate={endDate}
                    minDate={startDate}
                    withPortal
                    fixedHeight
                    customInput={<EndInput />}
                />
                </section>
                {/* <p className="card-title2"><span>Продажи:</span> {valSumm.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ')} ₴</p>
                <p className="card-title2"><span>Безнал:</span> {bankSumm.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ')} ₴</p>
                <p className="card-title2"><span>Скидка:</span> {skidkaSumm.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ')} ₴</p> */}
                <Widget title1="Сумма" title2="Продаж" text="общая сумма продаж" value={valSumm ? hhh : 'нет данных'} subvalue={'Безн.: ' + hhh2 + '  |  ' + 'Скид.: ' + hhh3}/>
                <div className = "App">
                <div className="stroka2">
                                <div className="text2">
                                    
                                    <div className="info-tovary">
                                        <div className="zagolovok-tovary"><span className="ff">Название</span></div>
                                        <div className="summa-tovary"><span className="ff">Сумма</span></div>
                                        <div className="summa-tovary"><span className="ff">Безнал</span></div>
                                        <div className="summa-tovary"><span className="ff">Скидка</span></div>
                                    </div>
                                </div>
                                <svg className="MuiSvgIcon-root" focusable="false" viewBox="0 0 24 24" aria-hidden="true" role="presentation"></svg>
                            </div>
                  {
                    Object.keys(result).map((item, index) =>
                      <SkladTableRow key={index} keys={index} magaziny={item} datamag={result[item]}/>
                    )
                  }
                  
                </div>
              </div>
            );
          }else if (error !== null){
            return (
              
              <div className="zakazy">
                <Link className = "link-item" to="/magaziny">
                    <div className="back">
                        {back}
                    </div>
                </Link>
                <p className="card-title-zakaz"><span>Ошибка соединения !</span></p>
                <div className = "App">
                  <p className="net-zakazov"><span>Отсутсвует интернет</span></p>
                </div>
              </div>
            );
          }

        return(
            <div className="conteiner-list">
                <Link className = "link-item" to="/magaziny">
                    <div className="back">
                        {back}
                    </div>
                </Link>
                <p className="card-title"><span>Данные</span> за период</p>
                <section className="date-section">
                <DatePicker 
                    selected={startDate} 
                    onChange={this.handleChangeStart} 
                    maxDate={new Date()} 
                    locale="ru"
                    dateFormat="dd/MM/yyyy"
                    todayButton="Сегодня"
                    selectsStart
                    startDate={startDate}
                    endDate={endDate}
                    withPortal
                    fixedHeight
                    customInput={<StartInput />}
                />
                <DatePicker 
                    selected={endDate} 
                    onChange={this.handleChangeEnd}
                    maxDate={new Date()} 
                    locale="ru"
                    dateFormat="dd/MM/yyyy"
                    todayButton="Сегодня"
                    selectsEnd
                    endDate={endDate}
                    minDate={startDate}
                    withPortal
                    fixedHeight
                    customInput={<EndInput />}
                />
                </section>
                <div  className="text-center"><em className="text-muted">Загрузка данных...</em></div>
                <img className = "loading" src={loading}/>              
            </div>  
        )
    };
    handleChangeStart = startDate => {
        const d1 = new Date(startDate)-100;
        this.setState({
          startDate: d1
        });
      };
      handleChangeEnd = endDate => {
        const d2 = new Date(endDate)-100;
        this.setState({
          endDate: d2
        });
      };
}
export default Magaziny;