import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import loading from '../../img/Loading.gif';
import './Abc.css';

class Abc extends Component {
    constructor() {
        super();
        this.state = {
            startDate: new Date(),
            endDate: new Date(),
            value: [],
            isLoading: true,
            error: null,
            
        };
    };
    zapros = () => {
        this.setState({isLoading: true});
        const datee = this.state.endDate -(1000);
        const dateb = this.state.startDate -(7*24*60*60*1000);
        
        //const ZakazDate = dd.getFullYear() + '-' + ('0' + (dd.getMonth() + 1)).slice(-2) + '-' + ('0' + dd.getDate()).slice(-2);
        const abc = &api + '/pkk/hs/pkk/abc/?dateb='+ dateb +'&datee='+ datee;
        fetch(abc)
          .then(response => {
            if (response.ok) {
              return response.json();
            } else {
              throw new Error('Что-то пошло нетак ...');
            }
          })
          .then(data => this.setState({ value: data.value, isLoading: false }))
          .catch(error => this.setState({ error, isLoading: false }));
    };
    obrabotka = (value) => {
        var objA = [];
        var objB = [];
        var objC = [];
        var dolyaNakopit = 0;
        const lastItem = value.length - 1;
        
        for (var i = 0; i < lastItem; i++){
            const dolya = (((value[i].СуммаВаловойПрибыли|0)/(value[lastItem].СуммаВаловойПрибыли|0))*100);/*.toFixed(2)*/
            dolyaNakopit = dolyaNakopit + dolya;
            if (dolyaNakopit <= 80){
                objA.push({'Name': value[i].Номенклатура, 'Pribyl': value[i].СуммаВаловойПрибыли|0, 'Koeficient': 'A'});
            };
            if (dolyaNakopit > 80 && dolyaNakopit <= 95){
                objB.push({'Name': value[i].Номенклатура, 'Pribyl': value[i].СуммаВаловойПрибыли|0, 'Koeficient': 'B'});
            };
            if (dolyaNakopit > 95){
                objC.push({'Name': value[i].Номенклатура, 'Pribyl': value[i].СуммаВаловойПрибыли|0, 'Koeficient': 'C'});
            };
        };
        return (
            <div className="123">
                <p>Товар группы 'А' приносит 80% прибыли</p>
                <div className="grid-table2">
                    <div className="z1">Наименование</div>
                    <div className="z1">Прибыль</div>
                    <div className="z1">Группа</div>
                    {objA.map((item, index) => [
                        <div key={index} className="kolonka-telo t1 jj">{item.Name}</div>,
                        <div key={index + 'a'} className="kolonka-telo2 t2 jj">{item.Pribyl.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ')} грн</div>,
                        <div key={index + 'b'} className="kolonka-telo3 t3 jj">{item.Koeficient}</div>  
                    ])}
                </div>
                <p>Товар группы 'B' приносит 15% прибыли</p>
                <div className="grid-table2">
                    <div className="z1">Наименование</div>
                    <div className="z1">Прибыль</div>
                    <div className="z1">Группа</div>
                    {objB.map((item, index) => [
                        <div key={index} className="kolonka-telo t1 jj">{item.Name}</div>,
                        <div key={index + 'a'} className="kolonka-telo2 t2 jj">{item.Pribyl.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ')} грн</div>,
                        <div key={index + 'b'} className="kolonka-telo3 t3 jj">{item.Koeficient}</div>  
                    ])}
                </div>  
                <p>Товар группы 'C' приносит 5% прибыли</p>
                <div className="grid-table2">
                    <div className="z1">Наименование</div>
                    <div className="z1">Прибыль</div>
                    <div className="z1">Группа</div>
                    {objC.map((item, index) => [
                        <div key={index} className="kolonka-telo t1 jj">{item.Name}</div>,
                        <div key={index + 'a'} className="kolonka-telo2 t2 jj">{item.Pribyl.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ')} грн</div>,
                        <div key={index + 'b'} className="kolonka-telo3 t3 jj">{item.Koeficient}</div>  
                    ])}
                </div> 
              
            </div>
        )
         
        
    };
    
    componentDidMount() {
        this.zapros();
    };
  
    componentDidUpdate(prevProps,prevState) {
        if (this.state.startDate.toDateString() !== prevState.startDate.toDateString() || this.state.endDate.toDateString() !== prevState.endDate.toDateString()) {
            this.zapros();
          }
    };
 
    render() {
        const {value, isLoading, error } = this.state;
        //Вывод сообщения в случае ошибки
        if(error !== null){
        return (
            <>
            <h1>Warning !</h1>
            <p>Connection failed</p>
            <p>You have problem with internet</p>
            <p>Server is not response</p>
            </>
        )
        };
        if(value.length > 0){
            var fff = this.obrabotka(value);
        }
        const back = '<  Вернуться Назад';
        return(
            <div className="conteiner-list">
                
                <Link className = "link-item" to="/analys">
                    <div className="back">
                        {back}
                    </div>
                </Link>
                <p className="card-title-zakaz"><span>Анализ</span> ABC</p>
                <span className="period-zakaz">данные берутся за недельный период</span>
                {fff !== undefined ? fff : [<span className="an">Проводится анализ...</span>,<img className = "loading" src={loading}/>]}
            </div>  
        )
    };
}
export default Abc;