import React from 'react';
//import { render } from 'react-dom';
import { slideDown, slideUp } from './anim';
import Button from '@material-ui/core/Button';
import loading from '../../img/Loading.gif';
//import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import './Sklad.css';

class SkladTableRow extends React.Component {
  state = { expanded: false }
  toggleExpander = (e) => {
    if (!this.state.expanded) {
      this.setState(
        { expanded: true },
        () => {
          if (this.refs.expanderBody) {
            slideDown(this.refs.expanderBody);
          }
        }
      );
    } else {
      slideUp(this.refs.expanderBody, {
        onComplete: () => { this.setState({ expanded: false }); }
      });
    }
  }

  render() {
    const {keys, datakat, kategory, mag } = this.props;
    return [
      <Button key={keys} onClick={this.toggleExpander}  className="buttontext">
        <span className="spanlabel">
          <h5 className="kategory">{kategory}</h5>
          <svg className="MuiSvgIcon-root" focusable="false" viewBox="0 0 24 24" aria-hidden="true" role="presentation"><path d="M16.59 8.59L12 13.17 7.41 8.59 6 10l6 6 6-6z"></path></svg>
        </span>
      </Button>,
      this.state.expanded && (
        <div className="expandable" key="tr-expander">
          <div className="uk-background-muted">
            <div ref="expanderBody" className="inner">
            <table className="table-responsive">
              <thead>
              <tr className="table-stroka-zagolovok">
                <th className="table-kolonka1-zagolovok">Наименование</th>
                {mag.map((item, index)=><th key={index} className="table-kolonki-zagolovok">{item}</th>)}
              </tr>
              </thead>
              <tbody>
              {Object.keys(datakat).map((item, index) => 
                <tr key={index} className="table-stroka-telo">
                  <td className="table-kolonka1-telo">{item}</td>
                  {mag.map((item2, index2)=>
                    <td key={index2} className={datakat[item][item2] < 0 ? "table-kolonki-telo red" : "table-kolonki-telo"}>
                      {datakat[item][item2] === undefined ? 0 : datakat[item][item2]}
                    </td>)
                  }
                </tr>)}
              </tbody>
            </table>     
            </div>
          </div>
        </div>
      )
    ];
  }
}

class Sklad extends React.Component {
  state = { sklad: [],
            skladCentre: [] }

  componentDidMount() {
    fetch(api + '/pkk/hs/pkk/pkr')
      .then(response => response.json())
      .then(data => { this.setState({sklad: data.value}) });
    fetch(api + '/pkk/hs/pkk/pkrsklad')
      .then(response => response.json())
      .then(data => { this.setState({skladCentre: data.value}) });
  }

  render() {
    const { sklad, skladCentre } = this.state;
    
    if(sklad.length > 0 && skladCentre.length > 0){
      var grid = {};
      const uniMag = [...new Set(sklad.map(item => item.Магазины))];
      const array4 = [skladCentre[0].Магазины, ...uniMag];
      const array3 = [...skladCentre, ...sklad];
      array3.map((item, index) => {
        if(item.Категории !== 'Продукция' 
        && item.Категории !== 'Архив' 
        && item.Категории !== 'АРХИВ ПИВО' 
        && item.Категории !== 'АРХИВ КОФЕ/ЧАЙ' 
        && item.Категории !== 'АРХИВ ЗАКУСКА' 
        && item.Категории !== '12.Кофе Чай' 
        && item.Категории !== '13.ОБОРУДОВАНИЕ' 
        && item.Категории !== '14.АРХИВ' 
        && item.Категории !== 'АРХИВ ВИНО' 
        && item.Категории !== '13.Оборудование' 
        && item.Категории !== '17.АРХИВ' 
        && item.Категории !== '10.Тара' 
        && item.Категории !== '14. Хоз. товары'){
          var kategory = item.Категории;
          if(kategory in grid === false){
              grid[kategory] = {};
          };
          var tovary = item.Товары;
          if(tovary in grid[kategory] === false){
              grid[kategory][tovary] = {};
          };
          grid[kategory][item.Товары][item.Магазины] = item.Количество;// + item.ЕдиницаИзмерения;
        }
      })
      return (
        <div className="conteiner-list">
          <p className="card-title"><span>Таблица</span> Остатков</p>
          {
            Object.keys(grid).map((item, index) =>
              <SkladTableRow key={index} keys={index} kategory={item} datakat={grid[item]} mag={array4}/>
            )
          }
        </div>  
      );
    };
    // const uniKategory = [...new Set(sklad.map(item => item.Категории))];
    return (
      <div className="conteiner-list">
        <p className="card-title"><span>Таблица</span> Остатков</p>
        <div  className="text-center"><em className="text-muted">Загрузка остатков...</em></div>
        <img className = "loading" src={loading}/>
      </div>  
    );
  }
}
export default Sklad;
