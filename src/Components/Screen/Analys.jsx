import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import './Analys.css';

class Analys extends Component {
    constructor() {
        super();
        this.state = {
            value: [],
            isLoading: false,
            error: null,
        };
    };
    componentDidMount() {
       
    };
  
    componentDidUpdate(prevProps,prevState) {
    
    };
 
    render() {
        const { value, isLoading, error } = this.state;
        //Вывод сообщения в случае ошибки
        if(error !== null){
        return (
            <>
            <h1>Warning !</h1>
            <p>Connection failed</p>
            <p>You have problem with internet</p>
            <p>Server is not response</p>
            </>
        )
        };

        return(
            <div className="conteiner-list">
                <p className="card-title-zakaz"><span>Выберите</span> отчёт:</p>
                <Link className = "link-item" to="/analys/abc">
                    <div className="list-analys">
                        ABC-Анализ
                    </div>
                </Link>
                <Link className = "link-item" to="/analys/xyz">
                    <div className="list-analys">
                        XYZ-Анализ
                    </div>
                </Link>
                <Link className = "link-item" to="/analys/xyz">
                    <div className="list-analys">
                        Продажи: План-Факт
                    </div>
                </Link>
            </div>  
            
        )
    };
}
export default Analys;