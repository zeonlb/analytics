import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import './Prodazhy.css';
import DatePicker from "react-datepicker";
import loading from '../../img/Loading.gif';
import { slideDown, slideUp } from './anim';
import Button from '@material-ui/core/Button';
import { registerLocale } from  "react-datepicker";
import ru from 'date-fns/locale/ru';

import './TovaryMagaz.css';
registerLocale('ru', ru);

class SkladTableRow extends React.Component {
  state = { expanded: false }
  toggleExpander = (e) => {
    if (!this.state.expanded) {
      this.setState(
        { expanded: true },
        () => {
          if (this.refs.expanderBody) {
            slideDown(this.refs.expanderBody);
          }
        }
      );
    } else {
      slideUp(this.refs.expanderBody, {
        onComplete: () => { this.setState({ expanded: false }); }
      });
    }
  }

  render() {
    const {keys, datakat, kategory } = this.props;
    var SummDocum = datakat['SumIrarh'];
    var datakat2 = JSON.parse(JSON.stringify(datakat));
    delete datakat2['SumIrarh'];
    return [
      <Button key={keys} onClick={this.toggleExpander} className="buttontext">
        <div className="stroka2">
            <div className="text2">
                <div className="info-tovary">
                    <div className="zagolovok-tovary"><span className="imya2">{kategory}</span></div>
                    <div className="summa-tovary"><span className="chislo">{SummDocum.toFixed(2).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ') + " ₴"}</span></div>
                </div>
            </div>
            <svg className="MuiSvgIcon-root" focusable="false" viewBox="0 0 24 24" aria-hidden="true" role="presentation"><path d="M16.59 8.59L12 13.17 7.41 8.59 6 10l6 6 6-6z"></path></svg>
        </div>
      </Button>,
      this.state.expanded && (
        <div className="expandable" key="tr-expander">
          <div  className="uk-background-muted">
            <div ref="expanderBody" className="inner">
            <div className="grid-table5">
              <div className="kolonka zagolovok1">Наименование</div>
              <div className="kolonka zagolovok2">Кол-во</div>
              <div className="kolonka zagolovok3">Цена</div>
              <div className="kolonka zagolovok4">Сумма</div>            
              
              {Object.keys(datakat2).map((item, index) => [
                  <div key={index} className="kolonka telo1">{item}</div>,
                  <div className="kolonka telo2">{datakat[item].Koll}</div>,
                  <div className="kolonka telo3">{datakat[item].Cena}</div>,
                  <div className="kolonka telo4">{datakat[item].Summa.toFixed(2).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ') + " ₴"}</div>
                  ])}
            </div> 
               
            </div>
          </div>
        </div>
      )
    ];
  }
}

class TovaryMagaz extends Component {
  constructor() {
    super();
    this.state = {
      value: [],
      isLoading: true,
      error: null,
      SaleDate: new Date(),
      SaleDate2: new Date()
    };
  }
  
  zapros = () => {
    this.setState({isLoading: true});
    const dd = this.state.SaleDate -(1000);
    const bb = this.state.SaleDate2 -(1000);
    const SALE = api + '/pkk/hs/pkk/tovmagaz/?dateb=' + dd + '&datee=' + bb;
   
    fetch(SALE)
      .then(response => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error('Что-то пошло нетак ...');
        }
      })
      .then(data => this.setState({ value: data.value, isLoading: false }))
      .catch(error => this.setState({ error, isLoading: false }));
  }

  componentDidMount() {
    this.zapros();
  };
  
  componentDidUpdate(prevProps,prevState) {
    if (this.state.SaleDate.toDateString() !== prevState.SaleDate.toDateString() || this.state.SaleDate2.toDateString() !== prevState.SaleDate2.toDateString()) {
      this.zapros();
    }
  }

  render() {
    const { value, isLoading, error, SaleDate, SaleDate2} = this.state;
    const back = '< Назад';
    const StartInput = ({ value, onClick }) => (
      <div className="date-box">
        <span>от:</span>
        <button className="box-input" onClick={onClick}><span></span>
          {value}
        </button>
      </div>
    );
    const EndInput = ({ value, onClick }) => (
    <div className="date-box">
        <span>до:</span>
        <button className="box-input" onClick={onClick}><span></span>
        {value}
        </button>
    </div>
    );

    if(value.length > 0 && isLoading === false){
      var grid = {};
      var Magazin;
      var summaIrarhia = 'SumIrarh';
      var Tovar;
      var date = 0;

      value.map((item, index) => {
        Magazin = item.СкладНаименование;
        if(Magazin in grid === false){
            grid[Magazin] = {};
        };
        if(summaIrarhia in grid[Magazin] === false){
          grid[Magazin][summaIrarhia] = {};
          grid[Magazin][summaIrarhia] = 0;
          date = 0;
        };
        if(date !== item.Дата){
          grid[Magazin][summaIrarhia] = grid[Magazin][summaIrarhia] + item.СуммаДокумента;
          date = item.Дата;
        }
        
        Tovar = item.ТоварыНоменклатураНаименование;
        if(Tovar in grid[Magazin] === false){
            grid[Magazin][Tovar] = {};
            grid[Magazin][Tovar] = {
              Koll: 0,
              Summa: 0,
              Cena: item.ТоварыЦена
            };
            
        };
        grid[Magazin][Tovar].Koll = grid[Magazin][Tovar].Koll + item.ТоварыКоличество;
        grid[Magazin][Tovar].Summa = grid[Magazin][Tovar].Summa + item.ТоварыСумма;
      });
      
      var result = {};
      Object.keys(grid).sort(function (a, b) {
          return grid[b].SumIrarh - grid[a].SumIrarh;
      }).forEach(function (v) { result[v] = grid[v]; });
      grid = result;
      return (
        <div className="zakazy">
          <Link className = "link-item" to="/magaziny">
                    <div className="back">
                        {back}
                    </div>
                </Link>
          <p className="card-title-zakaz"><span>Перечень</span> проданных товаров</p>
          <section className="date-section">
            <DatePicker 
              selected={SaleDate} 
              onChange={this.handleChangeSaleDate} 
              maxDate={new Date()} 
              locale="ru"
              dateFormat="dd/MM/yyyy"
              todayButton="Сегодня"
              selectsStart
              startDate={SaleDate}
              endDate={SaleDate}
              withPortal
              fixedHeight
              customInput={<StartInput />}
            />
            <DatePicker 
              selected={SaleDate2} 
              onChange={this.handleChangeSaleDate2} 
              maxDate={new Date()} 
              locale="ru"
              dateFormat="dd/MM/yyyy"
              todayButton="Сегодня"
              selectsStart
              startDate={SaleDate2}
              endDate={SaleDate2}
              withPortal
              fixedHeight
              customInput={<EndInput />}
            />
          </section>
          <div className = "App">
            {
              Object.keys(grid).map((item, index) =>
                <SkladTableRow key={index} keys={index} kategory={item} datakat={grid[item]}/>
              )
            }
          </div>
        </div>
      );
    }else if (error !== null){
      return (
        <div className="zakazy">
          <Link className = "link-item" to="/magaziny">
                    <div className="back">
                        {back}
                    </div>
                </Link>
          <p className="card-title-zakaz"><span>Ошибка соединения !</span></p>
          <div className = "App">
            <p className="net-zakazov"><span>Отсутсвует интернет</span></p>
          </div>
        </div>
      );
    }
    return (
      <div className="conteiner-list">
        <Link className = "link-item" to="/magaziny">
                    <div className="back">
                        {back}
                    </div>
                </Link>
        <p className="card-title-zakaz"><span>Перечень</span> проданных товаров</p>
        <section className="date-section">
            <DatePicker 
              selected={SaleDate} 
              onChange={this.handleChangeSaleDate} 
              maxDate={new Date()} 
              locale="ru"
              dateFormat="dd/MM/yyyy"
              todayButton="Сегодня"
              selectsStart
              startDate={SaleDate}
              endDate={SaleDate}
              withPortal
              fixedHeight
              customInput={<StartInput />}
            />
            <DatePicker 
              selected={SaleDate2} 
              onChange={this.handleChangeSaleDate2} 
              maxDate={new Date()} 
              locale="ru"
              dateFormat="dd/MM/yyyy"
              todayButton="Сегодня"
              selectsStart
              startDate={SaleDate2}
              endDate={SaleDate2}
              withPortal
              fixedHeight
              customInput={<EndInput />}
            />
          </section>
        <div  className="text-center"><em className="text-muted">Загрузка истории...</em></div>
        <img className = "loading" src={loading}/>
      </div>  
    );
  }
  handleChangeSaleDate = SaleDate => {
    this.setState({
      SaleDate
    });
  };
  handleChangeSaleDate2 = SaleDate2 => {
    this.setState({
      SaleDate2
    });
  };
  
}

export default TovaryMagaz;