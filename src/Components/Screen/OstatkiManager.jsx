import React from 'react';
//import { render } from 'react-dom';
import { slideDown, slideUp } from './anim';
import Button from '@material-ui/core/Button';
import loading from '../../img/Loading.gif';
//import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import './Sklad.css';
import './OstatkiManager.css';

class SubTableRow extends React.Component {
  state = { expanded: false }
  toggleExpander = (e) => {
    if (!this.state.expanded) {
      this.setState(
        { expanded: true },
        () => {
          if (this.refs.expanderBody) {
            slideDown(this.refs.expanderBody);
          }
        }
      );
    } else {
      slideUp(this.refs.expanderBody, {
        onComplete: () => { this.setState({ expanded: false }); }
      });
    }
  }
  render() {
    const {keys, stylecss, kategoryname, blockkategory} = this.props;
    var indikator;
    if(stylecss === 0 || stylecss === undefined){
      indikator = 'grays';
    }else{
      indikator = '';
    }
    return [
      <Button key={keys} onClick={this.toggleExpander}  className="buttontext">
        <span className={"sl-spanlabel " + indikator}>
          <h5 className="sl-kategory">{kategoryname}</h5>
          <svg className="MuiSvgIcon-root" focusable="false" viewBox="0 0 24 24" aria-hidden="true" role="presentation"><path d="M16.59 8.59L12 13.17 7.41 8.59 6 10l6 6 6-6z"></path></svg>
        </span>
      </Button>,
      this.state.expanded && (
        <div className="expandable" key="tr-expander">
          <div className="uk-background-muted">
            <div ref="expanderBody" className="sl-inner">
              <div className="sl-grid-table">
                <div className="kolonka1 sl-zagolovok1">Наименование</div>
                <div className="kolonka1 sl-zagolovok2">Наличие</div>
                <div className="kolonka1 sl-zagolovok3">Заказ</div>
                {Object.keys(blockkategory).map((item, index) => [
                    <div key={index} className="sl-kolonka telo1">{item}</div>,
                    <div key={index} className="sl-kolonka telo1">{blockkategory[item].Ostatok}</div>,
                    <div key={index} className="sl-kolonka telo1">{blockkategory[item].Zakaz}</div>,
                    ])}
              </div>
            </div>
          </div>
        </div>
      )
    ];
  }
}

class SkladTableRow extends React.Component {
  state = { expanded: false }
  toggleExpander = (e) => {
    if (!this.state.expanded) {
      this.setState(
        { expanded: true },
        () => {
          if (this.refs.expanderBody) {
            slideDown(this.refs.expanderBody);
          }
        }
      );
    } else {
      slideUp(this.refs.expanderBody, {
        onComplete: () => { this.setState({ expanded: false }); }
      });
    }
  }
  render() {
    const {keys, sklady, magname, magaziny, stylecss} = this.props;
    let zakazmagaziny = {};
    let ostatok = 'Ostatok';
    let zakaz = 'Zakaz';
    Object.keys(sklady).map((item, index) => {
      // console.log(magaziny);
      Object.keys(sklady[item]).map((item2, index2) => {
        //console.log(item + item2 + sklady[item][item2]);
        if(item in zakazmagaziny === false){
          zakazmagaziny[item] = {};
        };
        if(item2 in zakazmagaziny[item] === false){
          zakazmagaziny[item][item2] = {};
        };
        if(ostatok in zakazmagaziny[item][item2] === false){
          zakazmagaziny[item][item2][ostatok] = {};
        };
        if(zakaz in zakazmagaziny[item][item2] === false){
          zakazmagaziny[item][item2][zakaz] = {};
        };
        zakazmagaziny[item][item2][ostatok] = sklady[item][item2];
        if (magaziny !== undefined){
          if(magaziny[item] !== undefined){
            zakazmagaziny[item][item2][zakaz] = magaziny[item][item2] ? magaziny[item][item2] : '---';
          }else{
            zakazmagaziny[item][item2][zakaz] = '---';
          }
          
        }else{
          zakazmagaziny[item][item2][zakaz] = '---';
        }
        
      });
    });
    
    return [
      <Button key={keys} onClick={this.toggleExpander}  className="buttontext">
        {/* <span className="gl-spanlabel">stylecss */}
        <span className={stylecss ? "gl-spanlabel" : "gl-spanlabel-grey"}>
          <h5 className={stylecss ? "gl-kategory" : "gl-kategory grey"}>{magname}</h5>
          <svg className="MuiSvgIcon-root" focusable="false" viewBox="0 0 24 24" aria-hidden="true" role="presentation"><path d="M16.59 8.59L12 13.17 7.41 8.59 6 10l6 6 6-6z"></path></svg>
        </span>
      </Button>,
      this.state.expanded && (
        <div className="expandable" key="tr-expander">
          <div className="uk-background-muted">
            <div ref="expanderBody" className="gl-inner">
              <div className="conteiner-list">
                {Object.keys(zakazmagaziny).map((item, index) => [
                  <SubTableRow key={index} keys={index} kategoryname={item} blockkategory={zakazmagaziny[item]} stylecss={magaziny ? magaziny[item] : 0}/>
                    ])}
              </div>
            </div>
          </div>
        </div>
      )
    ];
  }
}

class OstatkiManager extends React.Component {
    constructor() {
        super();
        this.state = {
            sklad: [],
            zakazy: [],
            ZakazDate: new Date()
        };
      };
    componentDidMount() {
        const dd = this.state.ZakazDate - (500);
        const ZAKAZ = api + '/pkk/hs/pkk/zakazmanager/?dateb=' + dd;
        fetch(api + '/pkk/hs/pkk/pkr')
        .then(response => response.json())
        .then(data => { this.setState({sklad: data.value}) });
        fetch(ZAKAZ)
        .then(response => response.json())
        .then(data => { this.setState({zakazy: data.value}) });
    }

  render() {
    const { sklad, zakazy } = this.state;
    if(sklad.length > 0 && zakazy.length > 0){
      var grid = {};
      sklad.map((item, index) => {
        if(item.Категории !== 'Продукция' 
        && item.Категории !== 'Архив' 
        && item.Категории !== 'АРХИВ ПИВО' 
        && item.Категории !== 'АРХИВ КОФЕ/ЧАЙ' 
        && item.Категории !== 'АРХИВ ЗАКУСКА' 
        && item.Категории !== '12.Кофе Чай' 
        && item.Категории !== '13.ОБОРУДОВАНИЕ' 
        && item.Категории !== '14.АРХИВ' 
        && item.Категории !== 'АРХИВ ВИНО' 
        && item.Категории !== '13.Оборудование' 
        && item.Категории !== '17.АРХИВ' 
        && item.Категории !== '10.Тара' 
        && item.Категории !== 'КЕГИ' 
        && item.Категории !== 'Техника' 
        && item.Категории !== '14. Хоз. товары'){
          var magaziny = item.Магазины;
          if(magaziny in grid === false){
              grid[magaziny] = {};
          };
          var kategory = item.Категории;
          if(kategory in grid[magaziny] === false){
              grid[magaziny][kategory] = {};
          };
          var tovary = item.Товары;
          if(tovary in grid[magaziny][kategory] === false){
            grid[magaziny][kategory][tovary] = {};
          };
          grid[magaziny][kategory][tovary] = item.Количество + ' ' + item.ЕдиницаИзмерения;
        }
      });
      var grid2 = {};
      zakazy.map((item, index) => {
        if(item.ТоварыНоменклатураРодительНаименование !== 'Продукция' 
        && item.ТоварыНоменклатураРодительНаименование !== 'Архив' 
        && item.ТоварыНоменклатураРодительНаименование !== 'АРХИВ ПИВО' 
        && item.ТоварыНоменклатураРодительНаименование !== 'АРХИВ КОФЕ/ЧАЙ' 
        && item.ТоварыНоменклатураРодительНаименование !== 'АРХИВ ЗАКУСКА' 
        && item.ТоварыНоменклатураРодительНаименование !== '12.Кофе Чай' 
        && item.ТоварыНоменклатураРодительНаименование !== '13.ОБОРУДОВАНИЕ' 
        && item.ТоварыНоменклатураРодительНаименование !== '14.АРХИВ' 
        && item.ТоварыНоменклатураРодительНаименование !== 'АРХИВ ВИНО' 
        && item.ТоварыНоменклатураРодительНаименование !== '13.Оборудование' 
        && item.ТоварыНоменклатураРодительНаименование !== '17.АРХИВ' 
        && item.ТоварыНоменклатураРодительНаименование !== '10.Тара' 
        && item.ТоварыНоменклатураРодительНаименование !== 'КЕГИ' 
        && item.ТоварыНоменклатураРодительНаименование !== 'Техника' 
        && item.ТоварыНоменклатураРодительНаименование !== '14. Хоз. товары'){
          var magaziny = item.ЗаказчикНаименование;
          if(magaziny in grid2 === false){
            grid2[magaziny] = {};
          };
          var kategory = item.ТоварыНоменклатураРодительНаименование;
          if(kategory in grid2[magaziny] === false){
            grid2[magaziny][kategory] = {};
          };
          var tovary = item.ТоварыНоменклатураНаименование;
          if(tovary in grid2[magaziny][kategory] === false){
            grid2[magaziny][kategory][tovary] = {};
          };
          grid2[magaziny][kategory][tovary] = item.ТоварыКоличество + ' ' + item.ТоварыЕдиницаИзмеренияНаименование;
          if(grid[magaziny][kategory][tovary] === undefined){
            if(magaziny in grid === false){
              grid[magaziny] = {};
            };
            if(kategory in grid[magaziny] === false){
              grid[magaziny][kategory] = {};
            };
            if(tovary in grid[magaziny][kategory] === false){
              grid[magaziny][kategory][tovary] = {};
            };
            grid[magaziny][kategory][tovary] = 0 + ' ' + item.ТоварыЕдиницаИзмеренияНаименование;
          };
        };
      });
      return (
        <div className="conteiner-list">
          <p className="card-title"><span>Заказы и</span> Остатки</p>
          <p className="post"><span>Просмотр заказов возможен до момента отгрузки, после заказы можно просмотреть только в "Заказы и Отгрузки"</span></p>
          <p className="post"><span>Заказы Хоз.товаров не отображаются, т.к. по ним нет учета</span></p>
          {
            Object.keys(grid).map((item, index) =>
              <SkladTableRow key={index} keys={index} magname={item} sklady={grid[item]} magaziny={grid2[item]} stylecss={grid2[item] ? 1 : 0}/>
            )
          }
        </div>  
      );
    }else if (sklad.length > 0 && zakazy.length < 1){
      return (
        <div className="conteiner-list">
          <p className="card-title"><span>Заказы и</span> Остатки</p>
          <div  className="text-center"><em className="text-muted">Необработанных заказов нет !</em></div>
        </div>  
      );
    };
    return (
      <div className="conteiner-list">
        <p className="card-title"><span>Заказы и</span> Остатки</p>
        <div  className="text-center"><em className="text-muted">Загрузка остатков...</em></div>
        <img className = "loading" src={loading}/>
      </div>  
    );
  }
}
export default OstatkiManager;
