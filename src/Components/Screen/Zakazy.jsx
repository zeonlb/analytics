import React, { Component, useDebugValue } from 'react';
import './Prodazhy.css';
import DatePicker from "react-datepicker";
import loading from '../../img/Loading.gif';
import { slideDown, slideUp } from './anim';
import Button from '@material-ui/core/Button';
import { registerLocale } from  "react-datepicker";
import ru from 'date-fns/locale/ru';
import './Zakazy.css';
registerLocale('ru', ru);

class SkladTableRow extends React.Component {
  state = { expanded: false }
  toggleExpander = (e) => {
    if (!this.state.expanded) {
      this.setState(
        { expanded: true },
        () => {
          if (this.refs.expanderBody) {
            slideDown(this.refs.expanderBody);
          }
        }
      );
    } else {
      slideUp(this.refs.expanderBody, {
        onComplete: () => { this.setState({ expanded: false }); }
      });
    }
  }
  render() {
    const {keys, datakat, kategory } = this.props;
    
    var tZakaz;
    var tOtpravka;
    Object.keys(datakat).map((item, index) => {
      if(datakat[item].MomentZakaza !== undefined){
        tZakaz = datakat[item].MomentZakaza.split("; Внутренний заказ ");
      };
      if(datakat[item].MomentOtpravki !== undefined){
        tOtpravka = datakat[item].MomentOtpravki.split("; Перемещение товаров ");
      };
    });
    if(tZakaz === undefined && tOtpravka !== undefined){
      
      return [
        <Button key={keys} onClick={this.toggleExpander} className="buttontext">
          <span className="spanlabel">
            <h5 className="kategory">{kategory}</h5>
            <svg className="MuiSvgIcon-root" focusable="false" viewBox="0 0 24 24" aria-hidden="true" role="presentation"><path d="M16.59 8.59L12 13.17 7.41 8.59 6 10l6 6 6-6z"></path></svg>
          </span>
        </Button>,
        this.state.expanded && (
          <div className="expandable" key="tr-expander">
            <div  className="uk-background-muted">
              <div ref="expanderBody" className="inner">
                <p className="sub-str">Заказы отсутствуют</p> 
                <p className="sub-str-otgr">Отгрузка проведена: {tOtpravka[0]}</p>
                <div className="grid-table">
                  <div className="kolonka1 zagolovok1">Наименование</div>
                  <div className="kolonka1 zagolovok2">Заказ</div>
                  <div className="kolonka1 zagolovok3">Отгрузка</div>
                  <div className="kolonka1 zagolovok4">Ед.</div>
                  {Object.keys(datakat).map((item, index) => [
                      <div key={index} className="kolonka telo1">{item}</div>,
                      <div className="kolonka telo2"> --- </div>,
                      <div className="kolonka telo3">{datakat[item].KollOtpravka}</div>,
                      <div className="kolonka telo4">{datakat[item].Izmer}</div>
                      ])}
                </div> 
              </div>
            </div>
          </div>
        )
      ]
    }else if(tZakaz !== undefined && tOtpravka === undefined){

      return [
        <Button key={keys} onClick={this.toggleExpander} className="buttontext">
          <span className="spanlabel">
            <h5 className="kategory">{kategory}</h5>
            <svg className="MuiSvgIcon-root" focusable="false" viewBox="0 0 24 24" aria-hidden="true" role="presentation"><path d="M16.59 8.59L12 13.17 7.41 8.59 6 10l6 6 6-6z"></path></svg>
          </span>
        </Button>,
        this.state.expanded && (
          <div className="expandable" key="tr-expander">
            <div  className="uk-background-muted">
              <div ref="expanderBody" className="inner">
                <p className="sub-str">Заказ № <span>{tZakaz[1]}</span></p> 
                <p className="sub-str-otgr">Отгрузка не проводилась</p>
                <div className="grid-table">
                  <div className="kolonka1 zagolovok1">Наименование</div>
                  <div className="kolonka1 zagolovok2">Заказ</div>
                  <div className="kolonka1 zagolovok3">Отгрузка</div>
                  <div className="kolonka1 zagolovok4">Ед.</div>
                  {Object.keys(datakat).map((item, index) => [
                      <div key={index} className="kolonka telo1">{item}</div>,
                      <div className="kolonka telo2">{datakat[item].KollZakaz}</div>,
                      <div className="kolonka telo3"> --- </div>,
                      <div className="kolonka telo4">{datakat[item].Izmer}</div>
                      ])}
                </div> 
              </div>
            </div>
          </div>
        )
      ]
    }
    return [
      <Button key={keys} onClick={this.toggleExpander} className="buttontext">
          <span className="spanlabel">
            <h5 className="kategory">{kategory}</h5>
            <svg className="MuiSvgIcon-root" focusable="false" viewBox="0 0 24 24" aria-hidden="true" role="presentation"><path d="M16.59 8.59L12 13.17 7.41 8.59 6 10l6 6 6-6z"></path></svg>
          </span>
        </Button>,
        this.state.expanded && (
          <div className="expandable" key="tr-expander">
            <div  className="uk-background-muted">
              <div ref="expanderBody" className="inner">
                <p className="sub-str">Заказ № <span>{tZakaz[1]}</span></p> 
                <p className="sub-str-otgr">Отгрузка проведена: {tOtpravka[0]}</p>
                <div className="grid-table">
                  <div className="kolonka1 zagolovok1">Наименование</div>
                  <div className="kolonka1 zagolovok2">Заказ</div>
                  <div className="kolonka1 zagolovok3">Отгрузка</div>
                  <div className="kolonka1 zagolovok4">Ед.</div>
                  {Object.keys(datakat).map((item, index) => [
                      <div key={index} className="kolonka telo1">{item}</div>,
                      <div className={datakat[item].KollZakaz !== datakat[item].KollOtpravka ? "kolonka telo2 red" : "kolonka telo2 green"}>{datakat[item].KollZakaz !== undefined ? datakat[item].KollZakaz : '---'}</div>,
                      <div className={datakat[item].KollZakaz !== datakat[item].KollOtpravka ? "kolonka telo3 red" : "kolonka telo2 green"}>{datakat[item].KollOtpravka !== undefined ? datakat[item].KollOtpravka : '---'}</div>,
                      <div className="kolonka telo4">{datakat[item].Izmer !== undefined ? datakat[item].Izmer : 'n/a'}</div>
                      ])}
                </div> 
              </div>
            </div>
          </div>
        )
    ]
  }
}

class Zakazy extends Component {
  constructor() {
    super();
    this.state = {
      value: [],
      valueotgruz: [],
      isLoading: true,
      isLoadingOtgruz: true,
      error: null,
      ZakazDate: new Date()
    };
  }
  
  zapros = () => {
    this.setState({isLoading: true});
    const dd = this.state.ZakazDate -(24*60*60*1000);
   
    //const ZakazDate = dd.getFullYear() + '-' + ('0' + (dd.getMonth() + 1)).slice(-2) + '-' + ('0' + dd.getDate()).slice(-2);
    const ZAKAZ = api + '/pkk/hs/pkk/zakaz/?dateb='+dd;
   
    fetch(ZAKAZ)
      .then(response => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error('Что-то пошло нетак ...');
        }
      })
      .then(data => this.setState({ value: data.value, isLoading: false }))
      .catch(error => this.setState({ error, isLoading: false }));
    
    this.setState({isLoadingOtgruz: true});
    const tt = this.state.ZakazDate -(1000);
    const OTGRUZ = api + '/pkk/hs/pkk/otgruz/?dateb='+tt;
    
    fetch(OTGRUZ)
    .then(response => {
      if (response.ok) {
        return response.json();
      } else {
        throw new Error('Что-то пошло нетак ...');
      }
    })
    .then(data => this.setState({ valueotgruz: data.value, isLoadingOtgruz: false }))
    .catch(error => this.setState({ error, isLoadingOtgruz: false }));
  }

  componentDidMount() {
    this.zapros();
  };
  
  componentDidUpdate(prevProps,prevState) {
    if (this.state.ZakazDate.toDateString() !== prevState.ZakazDate.toDateString()) {
      this.zapros();
    }
  }

  render() {
    const { value, valueotgruz, isLoadingOtgruz, isLoading, error, ZakazDate} = this.state;
    
    const StartInput = ({ value, onClick }) => (
      <div className="date-box">
        <span>на дату:</span>
        <button className="box-input" onClick={onClick}><span></span>
          {value}
        </button>
      </div>
    );
    var grid2 = {};
    if(valueotgruz.length > 0){
      
      valueotgruz.map((item, index) => {
        var Magazin2 = item.Магазин;
        if(Magazin2 in grid2 === false){
            grid2[Magazin2] = {};
        };
        var Tovar2 = item.Номенклатура;
        if(Tovar2 in grid2[Magazin2] === false){
            grid2[Magazin2][Tovar2] = {};
        };
        grid2[Magazin2][Tovar2] = {
          Koll: item.КоличествоОтправка,
          Izmer: item.ЕдиницаИзмерения,
          Moment: item.МоментВремениОтправки
        };
      })
    };

    if(value.length > 0 && valueotgruz.length > 0 && isLoadingOtgruz === false && isLoading === false){
      const array4 = [...value, ...valueotgruz];
      var result = {};
      array4.map((item, index) => {
        var Magazin = item.Магазин;
        if(Magazin in result === false){
          result[Magazin] = {};
        };
        var Nomenklatura = item.Номенклатура;
        if(Nomenklatura in result[Magazin] === false){
          result[Magazin][Nomenklatura] = {};
        };
        
        if (item.КоличествоЗаказ !== undefined){
          result[Magazin][Nomenklatura]['KollZakaz'] = item.КоличествоЗаказ;
        };

        if (item.КоличествоОтправка !== undefined){
          result[Magazin][Nomenklatura]['KollOtpravka'] = item.КоличествоОтправка;
        };
        result[Magazin][Nomenklatura]['Izmer'] = item.ЕдиницаИзмерения;

        if (item.МоментВремениЗаказа !== undefined){
          result[Magazin][Nomenklatura]['MomentZakaza'] = item.МоментВремениЗаказа;
        };

        if (item.МоментВремениОтправки !== undefined){
          result[Magazin][Nomenklatura]['MomentOtpravki'] = item.МоментВремениОтправки;
        };
      });

      var grid = {};
      value.map((item, index) => {
        var Magazin = item.Магазин;
        if(Magazin in grid === false){
            grid[Magazin] = {};
        };
        if(index in grid[Magazin] === false){
            grid[Magazin][index] = {};
        };
        grid[Magazin][index] = {
          Tovar: item.Номенклатура,
          Koll: item.КоличествоЗаказ,
          Izmer: item.ЕдиницаИзмерения,
          Moment: item.МоментВремениЗаказа
        };
      })
      return (
        <div className="zakazy">
          <p className="card-title-zakaz"><span>История</span> заказов и отгрузок</p>
          <section className="date-section">
            <DatePicker 
              selected={ZakazDate} 
              onChange={this.handleChangeZakazDate} 
              maxDate={new Date()} 
              locale="ru"
              dateFormat="dd/MM/yyyy"
              todayButton="Сегодня"
              selectsStart
              startDate={ZakazDate}
              endDate={ZakazDate}
              withPortal
              fixedHeight
              customInput={<StartInput />}
            />
          </section>
          <div className = "App">
            {
              Object.keys(result).map((item, index) =>
                <SkladTableRow key={index} keys={index} kategory={item} datakat={result[item]}/>
              )
            }
          </div>
        </div>
      );
    }else if(error === null && isLoadingOtgruz === false && isLoading === false){
      
      return (
        <div className="zakazy">
          <p className="card-title-zakaz"><span>История</span> заказов и отгрузок</p>
          <section className="date-section">
            <DatePicker 
              selected={ZakazDate} 
              onChange={this.handleChangeZakazDate} 
              maxDate={new Date()} 
              locale="ru"
              dateFormat="dd/MM/yyyy"
              todayButton="Сегодня"
              selectsStart
              startDate={ZakazDate}
              endDate={ZakazDate}
              withPortal
              fixedHeight
              customInput={<StartInput />}
            />
          </section>
          <div className = "App">
            <p className="net-zakazov"><span>Заказов и Отгрузок нет в Базе</span></p>
          </div>
        </div>
      );
    }else if (error !== null){
      return (
        <div className="zakazy">
          <p className="card-title-zakaz"><span>Ошибка соединения !</span></p>
          <div className = "App">
            <p className="net-zakazov"><span>Отсутсвует интернет</span></p>
          </div>
        </div>
      );
    }
    return (
      <div className="conteiner-list">
        <p className="card-title-zakaz"><span>История</span> заказов и отгрузок</p>
        <section className="date-section">
            <DatePicker 
              selected={ZakazDate} 
              onChange={this.handleChangeZakazDate} 
              maxDate={new Date()} 
              locale="ru"
              dateFormat="dd/MM/yyyy"
              todayButton="Сегодня"
              selectsStart
              startDate={ZakazDate}
              endDate={ZakazDate}
              withPortal
              fixedHeight
              customInput={<StartInput />}
            />
          </section>
        <div  className="text-center"><em className="text-muted">Загрузка истории...</em></div>
        <img className = "loading" src={loading}/>
      </div>  
    );
  }
  handleChangeZakazDate = ZakazDate => {
    this.setState({
      ZakazDate
    });
  };
}
export default Zakazy;