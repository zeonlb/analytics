import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import './Zagl.css';

class Zagl extends Component {
  constructor() {
    super();
    this.state = {
     
    };
  }

  componentDidMount() {
   
  };
  
  componentDidUpdate(prevProps,prevState) {
   
  }

  render() {
    const back = '< Главное Меню';

        
    return (

      <div className = "warning">
         <div className="conteiner-list">
            
            <Link className = "link-item" to="/">
                <div className="back">
                    {back}
                </div>
            </Link>
        </div> 
     
        <h1 className="warning-h1">Доступ запрещен !</h1>
      
        <div className = "App">
            <p className = "App-text">Данная функция программы заблокирована</p>
            <p className = "App-text">Вернитесь в главное меню</p>
        </div>
      </div>
    );
  }
  
}

export default Zagl;