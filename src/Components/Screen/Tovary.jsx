import React, { Component } from 'react';
import loading from '../../img/Loading.gif';
import Widget from '../widget/widget';
//Выбор категорий
import InputLabel from '@material-ui/core/InputLabel';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import NativeSelect from '@material-ui/core/NativeSelect';
import InputBase from '@material-ui/core/InputBase';
//Календарь(выбор двух дат)
import DatePicker from "react-datepicker";
import { registerLocale } from  "react-datepicker";
import ru from 'date-fns/locale/ru';
// Анимация расскрытия списка
import { slideDown, slideUp } from './anim';
// Анимация нажатия на кнопку
import Button from '@material-ui/core/Button';

import './Tovary.css';
registerLocale('ru', ru);
const BootstrapInput = withStyles(theme => ({
    root: {
      'label + &': {
        marginTop: theme.spacing(3),
      },
    },
    input: {
        borderRadius: 4,
        color: '#fff',
        position: 'relative',
        backgroundColor: 'rgba(255, 255, 255, 0.05)',
        boxShadow: '0 2px 2px 0 rgba(0,0,0,0.14), 0 3px 1px -2px rgba(0,0,0,0.12), 0 1px 5px 0 rgba(0,0,0,0.2)',
        border: '1px solid #c49fff7a',
        fontSize: 16,
        padding: '10px 26px 10px 12px',
        '&:focus': {
            backgroundColor: '#263252',
        },
    },
  }))(InputBase);
class TovaryTableRow extends React.Component {
    state = { expanded: false}
    toggleExpander = (e) => {
      if (!this.state.expanded) {
        this.setState(
          { expanded: true },
          () => {
            if (this.refs.expanderBody) {
              slideDown(this.refs.expanderBody);
            }
          }
        );
      } else {
        slideUp(this.refs.expanderBody, {
          onComplete: () => { this.setState({ expanded: false }); }
        });
      }
    }
  
    render() {
        const {keys, kategory, datakat } = this.props;
        delete datakat['SumIrarh'];
        var zagolovokSumm = 0;
        var zagolovokKoll = 0;
        var zagolovokSebes = 0;
        var valovPribl = 0;
        var edIzmerenie = '';
        var grid2 = {};

        for (var key in datakat) {
            
            var summaMagazina = 0;
            var kollMagazina = 0;
            var Magazin = key;
            if(Magazin in grid2 === false){
                grid2[Magazin] = {};
            };
            for (var key2 in datakat[key]){
                zagolovokSumm = zagolovokSumm + datakat[key][key2].Stoimost;
                summaMagazina = summaMagazina + datakat[key][key2].Stoimost;
                kollMagazina = kollMagazina + datakat[key][key2].SummKolichestvo;
                zagolovokKoll = zagolovokKoll + datakat[key][key2].SummKolichestvo;
                zagolovokSebes = zagolovokSebes + datakat[key][key2].Sebestoimost;
                valovPribl = valovPribl + datakat[key][key2].ValovPribl;
                edIzmerenie = datakat[key][key2].izmerenie;
            };
            //summaMagazina = Math.round(summaMagazina).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ') + " ₴";
            summaMagazina = summaMagazina.toFixed(2).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ') + " ₴";
            //kollMagazina = Math.round(kollMagazina).toString() + " " + edIzmerenie;
            kollMagazina = kollMagazina.toFixed(2);
            grid2[Magazin] = {
                Summ: summaMagazina,
                Koll: kollMagazina,
                ed: edIzmerenie
            }
        };

        var nazenka = (valovPribl * 100 ) / zagolovokSebes;
        var rentabilnost = ((valovPribl / zagolovokSumm) * 100)|0;
        var cenaZaEd = zagolovokSumm / zagolovokKoll;
        var cenaZaEdSebestoimost = zagolovokSebes / zagolovokKoll;
        cenaZaEd = cenaZaEd.toFixed(2) + " ₴";
        //zagolovokKoll = Math.round(zagolovokKoll).toString() + " " + edIzmerenie;
        zagolovokKoll = zagolovokKoll.toFixed(1).toString() + " " + edIzmerenie;
        cenaZaEdSebestoimost = cenaZaEdSebestoimost.toFixed(2) + " ₴";
        nazenka = Math.round(nazenka).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ') + "%";
        zagolovokSumm = Math.round(zagolovokSumm).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ') + " ₴";
        rentabilnost = rentabilnost + '%';
        //zagolovokSebes = Math.round(zagolovokSebes).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ') + " ₴";
        //console.log(grid2);
        var result = {};
        Object.keys(grid2).sort(function (a, b) {
            return grid2[b].Koll - grid2[a].Koll;
        }).forEach(function (v) { result[v] = grid2[v]; });
        
        return [
            <Button key={keys} onClick={this.toggleExpander} className="buttontext">
                <div className="stroka2">
                    <div className="text2">
                        
                        <div className="info-tovary">
                            <div className="zagolovok-tovary"><span className="imya2">{kategory}</span></div>
                            <div className="summa-tovary"><span className="chislo">{zagolovokSumm}</span></div>
                            <div className="summa-tovary"><span className="chislo">{zagolovokKoll}</span></div>
                            <div className="summa-tovary"><span className="chislo">{rentabilnost}</span></div>
                        </div>
                    </div>
                    <svg className="MuiSvgIcon-root" focusable="false" viewBox="0 0 24 24" aria-hidden="true" role="presentation"><path d="M16.59 8.59L12 13.17 7.41 8.59 6 10l6 6 6-6z"></path></svg>
                </div>
            </Button>,
            this.state.expanded && (
                <div className="expandable" key="tr-expander">
                <div  className="uk-background-muted">
                    <div ref="expanderBody" className="inner">
                    <div className="summa"><span>Цена закупки (расчетная): </span><span className="chislo">{cenaZaEdSebestoimost}</span></div>
                    <div className="summa"><span>Цена продажи (расчетная): </span><span className="chislo">{cenaZaEd}</span></div>
                    <div className="summa"><span>Наценка: </span><span className="chislo">{nazenka}</span></div>
                    <div className="grid-table-tovary">
                        <div className="zag">Магазин</div>
                        <div className="zag">Кол-во</div>
                        <div className="zag">Сумма</div>            
                        
                        {Object.keys(result).map((item, index) => [
                            <div key={index} className="telo11">{item}</div>,
                            <div key={index + 'n'} className="telo22">{result[item].Koll} {result[item].ed}</div>,
                            <div key={index + 'm'} className="telo33">{result[item].Summ}</div>
                            ])}
                    </div> 
                    
                    </div>
                </div>
                </div>
            )
        ];
    }
}

class Tovary extends Component {
    constructor() {
        super();
        this.state = {
            value: [],
            catalog: [],
            isLoadingcatalog: true,
            kategory: '02.Пиво Разливное',
            startDate: new Date()-(1*24*60*60*1000),
            endDate: new Date()-(1000),
            isLoading: true,
            error: null,
        };
    };
  
    zapros = () => {
        this.setState({isLoading: true});
        const kategory = this.state.kategory;
        const startDate = this.state.startDate;
        const endDate = this.state.endDate;
        const API = api + '/pkk/hs/pkk/vpda2/?dateb=' + startDate + '&datee=' + endDate + '&kategory=' + kategory;
        fetch(API)
          .then(response => {
            if (response.ok) {
              return response.json();
            } else {
              throw new Error('Что-то пошло нетак ...');
            }
          })
          .then(data => this.setState({ value: data.value, isLoading: false }))
          .catch(error => this.setState({ error, isLoading: false }));    
      };

    componentDidMount() {
        const catalog = api + '/pkk/odata/standard.odata/Catalog_%D0%9D%D0%BE%D0%BC%D0%B5%D0%BD%D0%BA%D0%BB%D0%B0%D1%82%D1%83%D1%80%D0%B0?$format=json;odata=nometadata&$filter=Ref_Key%20eq%20guid%27a211570e-ea16-11e6-925c-782bcbd5bea6%27%20or%20Ref_Key%20eq%20guid%273871b191-568f-11e6-8b5a-782bcbd5bea6%27%20or%20Ref_Key%20eq%20guid%27fb494474-ff44-11e6-80bb-14dae9efaec7%27%20or%20Ref_Key%20eq%20guid%27aa8e4f4f-9a06-11e6-bd01-782bcbd5bea6%27%20or%20Ref_Key%20eq%20guid%275f65308e-0245-11e7-80bb-14dae9efaec7%27%20or%20Ref_Key%20eq%20guid%273871b193-568f-11e6-8b5a-782bcbd5bea6%27%20or%20Ref_Key%20eq%20guid%273871b19b-568f-11e6-8b5a-782bcbd5bea6%27%20or%20Ref_Key%20eq%20guid%273871b198-568f-11e6-8b5a-782bcbd5bea6%27%20or%20Ref_Key%20eq%20guid%273871b199-568f-11e6-8b5a-782bcbd5bea6%27%20or%20Ref_Key%20eq%20guid%275fa4f6a2-d830-11e7-9b8a-000c29c54281%27%20and%20%D0%90%D0%BA%D1%82%D1%83%D0%B0%D0%BB%D0%B5%D0%BD%D0%92%D0%A0%D0%BE%D0%B7%D0%BD%D0%B8%D1%86%D0%B5%20eq%20true%20and%20IsFolder%20eq%20true%20&$select=Description&$orderby=Description%20asc'
        fetch(catalog)
        .then(response => {
            if (response.ok) {
            return response.json();
            } else {
            throw new Error('Что-то пошло нетак ...');
            }
        })
        .then(data => this.setState({ catalog: data.value, isLoadingcatalog: false }))
        .catch(error => this.setState({ error, isLoadingcatalog: false }));
        this.zapros();
    };
  
    componentDidUpdate(prevProps,prevState) {
        if (this.state.startDate !== prevState.startDate || this.state.endDate !== prevState.endDate || this.state.kategory !== prevState.kategory) {
            this.zapros();
        }
    };
 
    render() {
        const { value, startDate, endDate, catalog, isLoadingcatalog, kategory, isLoading, error } = this.state;
        const StartInput = ({ value, onClick }) => (
            <div className="date-box">
              <span>от:</span>
              <button className="box-input" onClick={onClick}><span></span>
                {value}
              </button>
            </div>
        );
        const EndInput = ({ value, onClick }) => (
        <div className="date-box">
            <span>до:</span>
            <button className="box-input" onClick={onClick}><span></span>
            {value}
            </button>
        </div>
        );
        if(value.length > 0 && isLoading === false && catalog.length > 0 && isLoadingcatalog === false){
            var itogi = value[value.length-1];
            var vsego2 = Math.round(itogi.СтоимостьСНДС).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ') + " ₴";
            var vsego = Math.round(itogi.ВаловаяПрибыль).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ') + " ₴";
            var edIzm = value[0].НоменклатураБазоваяЕдиницаИзмеренияНаименование;
            var kollbasovyhedeniz = Math.round(itogi.КоличествоБазовыхЕдиниц).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ') + " " + edIzm;
            var grid = {};
            var grid2 = [];
            var GlSumm = 0;
            var summaIrarhia = 'SumIrarh';
            value.map((item, index) => {
                var tovar = item.Номенклатура;
                var magaz = item.РегистраторСкладНаименование;
                
                if(tovar !== ''){
                    if(tovar in grid === false){
                        grid[tovar] = {};
                    };
                    if(summaIrarhia in grid[tovar] === false){
                        grid[tovar][summaIrarhia] = {};
                        GlSumm = 0;
                    };
                    GlSumm = GlSumm + item.КоличествоБазовыхЕдиниц;
                    grid[tovar][summaIrarhia] = GlSumm;
                    if(magaz in grid[tovar] === false){
                        grid[tovar][magaz] = {};
                    };
                    if(index in grid[tovar][magaz] === false){
                        grid[tovar][magaz][index] = {};
                    };
                    grid[tovar][magaz][index] = {
                        Date: item.Период,
                        SummKolichestvo: item.КоличествоБазовыхЕдиниц,
                        Sebestoimost: item.СебестоимостьСНДС,
                        Stoimost: item.СтоимостьСНДС,
                        ValovPribl: item.ВаловаяПрибыль,
                        izmerenie: item.НоменклатураБазоваяЕдиницаИзмеренияНаименование
                    };
                };
            });
            var result = {};
            Object.keys(grid).sort(function (a, b) {
                return grid[b].SumIrarh - grid[a].SumIrarh;
            }).forEach(function (v) { result[v] = grid[v]; });
            
        }else if (error !== null){
            return (
                <div className="zakazy">
                    <p className="card-title-zakaz"><span>Ошибка соединения !</span></p>
                    <div className = "App">
                        <p className="net-zakazov"><span>Отсутсвует интернет</span></p>
                    </div>
                </div>
            );
        };
        
        return(
            <div className="conteiner-list">
                <p className="card-title"><span>Продажи</span> за период</p>
                <section className="date-section">
                <DatePicker 
                    selected={startDate} 
                    onChange={this.handleChangeStart} 
                    maxDate={new Date()} 
                    locale="ru"
                    dateFormat="dd/MM/yyyy"
                    todayButton="Сегодня"
                    selectsStart
                    startDate={startDate}
                    endDate={endDate}
                    withPortal
                    fixedHeight
                    customInput={<StartInput />}
                />
                <DatePicker 
                    selected={endDate} 
                    onChange={this.handleChangeEnd}
                    maxDate={new Date()} 
                    locale="ru"
                    dateFormat="dd/MM/yyyy"
                    todayButton="Сегодня"
                    selectsEnd
                    endDate={endDate}
                    minDate={startDate}
                    withPortal
                    fixedHeight
                    customInput={<EndInput />}
                />
                </section>
                <p className="card-title"><span>Категория</span> товара</p>
                <FormControl className="input-kategory">
                    
                    <NativeSelect
                    id="select-kategory"
                    value={kategory}
                    onChange={this.handleChange}
                    input={<BootstrapInput />}
                    >
                    {
                        catalog.map((item, index) =>
                            <option key={index} value={item.Description}>{item.Description}</option>
                        )
                    }
                    </NativeSelect>
                </FormControl>
                {isLoading === true ? 
                    <div>
                        <div  className="text-center"><em className="text-muted">Загрузка данных...</em></div>
                        <img className = "loading" src={loading}/>  
                    </div>
                    :
                    <div>
                        <Widget title1="Сумма" title2="Продаж" text="общая сумма категории" value={vsego2 ? vsego2 : 'нет данных'} subvalue={'Вал: ' + vsego + '   |   ' + 'Кол: ' + kollbasovyhedeniz}/>  
                        <div className = "App">
                            <div className="stroka2">
                                <div className="text2">
                                    
                                    <div className="info-tovary">
                                        <div className="zagolovok-tovary"><span className="ff">Название</span></div>
                                        <div className="summa-tovary"><span className="ff">Сумма</span></div>
                                        <div className="summa-tovary"><span className="ff">Кол-во</span></div>
                                        <div className="summa-tovary"><span className="ff">Рентаб.</span></div>
                                    </div>
                                </div>
                                <svg className="MuiSvgIcon-root" focusable="false" viewBox="0 0 24 24" aria-hidden="true" role="presentation"></svg>
                            </div>
                            {Object.keys(result).map((item, index) =>
                                <TovaryTableRow key={index} keys={index} kategory={item} datakat={result[item]}/>
                            )}
                        </div>
                    </div>
                }         
            </div>  
        )
    };
    handleChangeStart = startDate => {
        const d1 = new Date(startDate)-100;
        this.setState({
          startDate: d1
        });
      };
    handleChangeEnd = endDate => {
        const d2 = new Date(endDate)-100;
        this.setState({
            endDate: d2
        });
    };
    handleChange = event => {
        const d3 = event.target.value;
        this.setState({
            kategory: d3
        });
    };
    
}
export default Tovary;