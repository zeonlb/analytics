import React, { useState, useEffect  } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    
    flexShrink: 0,
  },
  headcol: {
    position:'absolute',
    width:'2em',
    left:'0'
      },
  h: {
    transform: 'rotate(-90deg)',
    width: '5px',
  },
  g: {
    'overflow-x':'scroll',  
    'margin-left': '2em'
  },
}));

export default function ControlledExpansionPanels() {
  const classes = useStyles();
  const [expanded, setExpanded] = useState(false);
  const [sklad, setSklad] = useState([]);
  const [hasError, setErrors] = useState(false);

  const API = api + '/pkk/hs/pkk/pkrsklad';

  useEffect(() =>{
    async function fetchData() {
      const res = await fetch(API);
      res
        .json()
        .then(res => setSklad(res.value))
        .catch(err => setErrors(err));
    }

    fetchData();
  }, []);
  
  const handleChange = panel => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

if(sklad.length !== 0){
let g = [...new Set(sklad.map(item => item.КатегорииСклад))];
console.log(g);
};
  return (
    <div className={classes.root}>
      <div className={classes.g}>
        
      </div>
    </div>
  );
}
