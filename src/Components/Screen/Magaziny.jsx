import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import './Analys.css';

class Magaz extends Component {
   
    componentDidMount() {
       
    };
  
    componentDidUpdate(prevProps,prevState) {
    
    };
 
    render() {
      
        return(
            <div className="conteiner-list">
                <p className="card-title-zakaz"><span>Выберите</span> отчёт:</p>
                <Link className = "link-item" to="/magaziny/sum">
                    <div className="list-analys">
                        По Суммам
                    </div>
                </Link>
                <Link className = "link-item" to="/magaziny/tov">
                    <div className="list-analys">
                        По Товарам
                    </div>
                </Link>
                
            </div>  
            
        )
    };
}
export default Magaz;