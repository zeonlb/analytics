import React from 'react';
import './widget.css';

function Widget(param) {
    var title1,title2,text1,value1,subvalue;
    param.title1 == null ? title1 = "Название" : title1 = param.title1;
    param.title2 == null ? title2 = "Виджета" : title2 = param.title2;
    param.text == null ? text1 = "Описание виджета" : text1 = param.text;
    param.value == null ? value1 = "00 000 ₴" : value1 = param.value;
    param.subvalue == null ? subvalue = "00% Отн.пр.года" : subvalue = param.subvalue;
    return ( 
    <div className = "widget">
        <p className="card-title"><span>{title1}</span> {title2}</p>
        <div className="card">
            <div className="card-metric-title">{text1}</div>
            <div className="card-metric-value">{value1}</div>
            <div className="card-metric-change">{subvalue}</div>
        </div>
    </div>
    );
}
export default Widget;