import React from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import './Menu.css';
function Menu() {
    return (
        <div className = "grid">
            <Link className = "link-item" to="/prodazhy">
                <div className = "item-menu">
                    <i className = "prodazhy"></i>
                    <p>Продажи</p>
                </div>
            </Link>
            <Link className = "link-item" to="/tovary">
            <div className = "item-menu">
                <i className = "balans"></i>
                <p>Товары</p>
            </div>
            </Link>
            <Link className = "link-item" to="/magaziny">
            <div className = "item-menu">
                <i className = "magaziny"></i>
                <p>Магазины</p>
            </div>
            </Link>
            <Link className = "link-item" to="/zakazy">
            <div className = "item-menu">
                <i className = "zakazy"></i>
                <p>Заказы-Отгрузки</p>
            </div>
            </Link>
            <Link className = "link-item" to="/ostatki">
                <div className = "item-menu">
                    <i className = "ostatki"></i>
                    <p>Остатки</p>
                </div>
            </Link>
            <Link className = "link-item" to="/debet">
            <div className = "item-menu">
                <i className = "debetorka"></i>
                <p>Дебиторка</p>
            </div>
            </Link>
            <Link className = "link-item" to="/analys">
            <div className = "item-menu">
                <i className = "analitika"></i>
                <p>Аналитика</p>
            </div>
            </Link>
            <Link className = "link-item" to="/OstatkiManager">
            <div className = "item-menu">
                <i className = "fin"></i>
                {/* <p>Фин.Отчет</p> */}
                <p>Заказ-Остаток</p>
            </div>
            </Link>
            <Link className = "link-item" to="/zagl">
            <div className = "item-menu">
                <i className = "nastroyki"></i>
                <p>Настройки</p>
            </div>
            </Link>
            <Link className = "link-item" to="/zagl">
            <div className = "item-menu">
                <i className = "kontrol"></i>
                <p>Контроль</p>
            </div>
            </Link>
        </div>      
    );
}
export default Menu;