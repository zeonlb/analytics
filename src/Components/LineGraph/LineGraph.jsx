import React, { PureComponent } from 'react'
import Chart from "chart.js";
import classes from './LineGraph.css';
let myLineChart;
//--Chart Style Options--//
Chart.defaults.global.defaultFontFamily = "'HelveticaNeueCyr', 'Roboto', sans-serif"
Chart.defaults.global.legend.display = true;
Chart.defaults.global.elements.line.tension = 0.5;
Chart.defaults.global.elements.point.radius = 2;
Chart.defaults.global.elements.point.pointStyle = 'circle';
Chart.defaults.global.elements.point.backgroundColor = 'rgba(255, 255, 255, 0.5)';
Chart.defaults.global.elements.rectangle.borderSkipped = 'bottom';
Chart.defaults.global.elements.rectangle.borderSkipped = 'top';
Chart.defaults.global.elements.rectangle.borderSkipped = 'left';
Chart.defaults.global.elements.rectangle.borderSkipped = 'right';
Chart.defaults.global.tooltips.intersect = false;
//Chart.defaults.global.pointLabels.fontColor = '#fff';
//--Chart Style Options--//
export default class LineGraph extends PureComponent {
    chartRef = React.createRef();

    componentDidMount() {
        this.buildChart();
    }

    componentDidUpdate() {
        this.buildChart();
    }

    buildChart = () => {
        const myChartRef = this.chartRef.current.getContext("2d");
        const { data1, data2, labels } = this.props;

        if (typeof myLineChart !== "undefined") myLineChart.destroy();

        myLineChart = new Chart(myChartRef, {
            type: "line",
            data: {
                labels: labels,
                datasets: [
                    {
                        label: "2018г",
                        data: data1,
                        fill: false,
                        backgroundColor: "#6610f221",
                        borderWidth: 2,
                        borderColor: "#6610f2" //"#6610f2"
                    },
                    {
                        label: "2019г",
                        data: data2,
                        fill: false,
                        backgroundColor: "#E0E0E021",
                        borderColor: "#E0E0E0",
                        borderWidth: 2
                    }
                ]
            },
            options: {
                //Customize chart options
                responsive: true,
                maintainAspectRatio: true,
                tooltips: {
                    displayColors: false,
                    titleFontSize: 16,
                    bodyFontSize: 14,
                    xPadding: 10,
                    yPadding: 10,
                    // callbacks: {
                    //     label: (tooltipItem, data) => {
                    //        return `${tooltipItem.value}  грн`
                    //     }
                    // }
                },
                legend: {
                    
                    labels: {
                        //usePointStyle:true,
                        fontColor: '#A8B1D2'
                    }
                },
                scales: {
                    yAxes: [{
                        gridLines: {
                            color: '#412271',
                            //borderDashOffset: 0.5,
                            //borderDash: [5,2],
                            drawBorder: false,
                            tickMarkLength: 10,
                            drawTicks: false,
                        },
                        ticks: {
                            fontColor: '#A8B1D2',
                            min: 0,
                            callback: function(value, index, values) {
                                return value/1000000 + 'M  ';
                            }
                            
                        }
                    }],
                    xAxes: [{
                        gridLines: {
                            display: true,
                            color: 'rgba(255, 255, 255, 0.1)',
                            //borderDashOffset: 0.5,
                            borderDash: [2,2],
                            drawBorder: false,
                            tickMarkLength: 10,
                            drawTicks: false,
                        },                
                        ticks: {
                            fontColor: '#A8B1D2',
                            callback: function(value, index, values) {
                                return value + '  ';
                            }
                            
                        }
                    }]
                }
            }
        });
    }
    render() {
        return (
            <div className = "widget">
                <p className="card-title"><span>Продажи</span> за год</p>
                <div className="card-metric-title">график продаж по месяцам</div>
                <div className={classes.graphContainer}>
                    <canvas
                        id="myChart"
                        ref={this.chartRef}
                    />
                </div>
            </div>  
        )
    }
}